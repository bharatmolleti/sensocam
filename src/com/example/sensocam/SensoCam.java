package com.example.sensocam;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.androidplot.Plot;
import com.androidplot.util.PlotStatistics;
import com.androidplot.util.Redrawer;
import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYStepMode;

public class SensoCam extends Activity implements View.OnClickListener,
		SurfaceHolder.Callback, SensorEventListener {

	private static final int CAMERA_QUALITY = CamcorderProfile.QUALITY_480P;
	private static final int SENSOR_RATE = SensorManager.SENSOR_DELAY_FASTEST;
	private static final int[] SENSORS_LOGGED = { Sensor.TYPE_GYROSCOPE };

	private static final String mNamePrefix = "videogyro_";
	private String mOutputPath = Environment.getExternalStorageDirectory()
			+ "/DCIM/SensoCam";
	private String mExtension = ".mp4";

	public static final String LOGTAG = "SENSOCAM";
	public static final String S_READY = "READY";
	public static final String S_RECORDING = "RECORDING...";

	private MediaRecorder recorder = null;
	private SurfaceHolder holder = null;
	private CamcorderProfile camcorderProfile = null;
	private Camera camera = null;
	private TextView mTextView = null;
	private TextView mTimeView = null;

	boolean recording = false;
	boolean usecamera = true;
	boolean previewRunning = false;

	// time display stuff
	double start_time = 0;
	final Handler handler = new Handler();

	// sensor stuff
	private DataOutputStream[] sensorStream = null;
	private SensorManager sensorMgr = null;
	private Sensor[] orSensor = null;
	private static final float NS2S = 1.0f / 1000000000.0f;
	private float timestamp;

	// graph overlay
	private static final int HISTORY_SIZE = 300;
	private XYPlot aprHistoryPlot = null;

	private SimpleXYSeries azimuthHistorySeries = null;
	private SimpleXYSeries pitchHistorySeries = null;
	private SimpleXYSeries rollHistorySeries = null;

	private Redrawer redrawer = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

		if (CamcorderProfile.hasProfile(CAMERA_QUALITY) == true) {
			camcorderProfile = CamcorderProfile.get(CAMERA_QUALITY);
			Log.d(LOGTAG, "Found the needed Profile.");
		} else {
			Log.e(LOGTAG, "Needed Profile not present on the device.");
			Toast.makeText(getApplicationContext(),
					"Needed Profile Not Present", Toast.LENGTH_LONG).show();
			finish();
		}

		if (false == createDirIfNotExists(mOutputPath)) {
			System.out.println("Failed to create the Needed Folder.");
			Log.e(LOGTAG, "Failed to create the Needed Folder.");
			Toast.makeText(getApplicationContext(),
					"Failed to create the Needed Folder.", Toast.LENGTH_LONG)
					.show();
			finish();
		}

		setContentView(R.layout.activity_sensocam);

		SurfaceView cameraView = (SurfaceView) findViewById(R.id.CameraView);
		holder = cameraView.getHolder();
		holder.addCallback(this);

		cameraView.setClickable(true);
		cameraView.setOnClickListener(this);

		orSensor = new Sensor[SENSORS_LOGGED.length];
		sensorStream = new DataOutputStream[SENSORS_LOGGED.length];

		// register for orientation sensor events
		sensorMgr = (SensorManager) getApplicationContext().getSystemService(
				Context.SENSOR_SERVICE);
		for (int i = 0; i < SENSORS_LOGGED.length; i++) {
			orSensor[i] = sensorMgr.getDefaultSensor(SENSORS_LOGGED[i]);
			// if we can't access the orientation sensor then exit
			if (orSensor[i] == null) {
				System.out.println("Failed to find the needed Sensor.");
				Log.e(LOGTAG, "Failed to find the needed Sensor.");
				Toast.makeText(getApplicationContext(),
						"Failed to find the needed Sensor.", Toast.LENGTH_LONG)
						.show();
				finish();
			}
		}

		aprHistoryPlot = (XYPlot) findViewById(R.id.aprHistoryPlot);
		azimuthHistorySeries = new SimpleXYSeries("Az.");
		azimuthHistorySeries.useImplicitXVals();
		pitchHistorySeries = new SimpleXYSeries("Pitch");
		pitchHistorySeries.useImplicitXVals();
		rollHistorySeries = new SimpleXYSeries("Roll");
		rollHistorySeries.useImplicitXVals();

		aprHistoryPlot.setPlotMargins(0, 0, 0, 0);
		aprHistoryPlot.setPlotPadding(0, 0, 0, 0);
		aprHistoryPlot.setDomainLabelWidget(null);
		aprHistoryPlot.setRangeLabelWidget(null);

		aprHistoryPlot.setBackgroundPaint(null);
		aprHistoryPlot.getGraphWidget().setBackgroundPaint(null);
		aprHistoryPlot.getGraphWidget().setGridBackgroundPaint(null);
		aprHistoryPlot.setBorderPaint(null);

		aprHistoryPlot.getGraphWidget().setDomainLabelWidth(0.0f);
		aprHistoryPlot.getGraphWidget().setRangeLabelWidth(0.0f);

		aprHistoryPlot.getGraphWidget().setDomainLabelPaint(null);
		aprHistoryPlot.getGraphWidget().setDomainOriginLabelPaint(null);

		aprHistoryPlot.getGraphWidget().setRangeLabelPaint(null);
		aprHistoryPlot.getGraphWidget().setRangeOriginLabelPaint(null);

		aprHistoryPlot.getGraphWidget().setDomainOriginLinePaint(null);
		aprHistoryPlot.getGraphWidget().setRangeOriginLinePaint(null);

		aprHistoryPlot.getLayoutManager().remove(
				aprHistoryPlot.getTitleWidget());

		aprHistoryPlot.getGraphWidget().getDomainGridLinePaint()
				.setColor(Color.TRANSPARENT);
		aprHistoryPlot.getGraphWidget().getRangeGridLinePaint()
				.setColor(Color.TRANSPARENT);
		aprHistoryPlot.getGraphWidget().getRangeSubGridLinePaint()
				.setColor(Color.TRANSPARENT);

		aprHistoryPlot.getLayoutManager().remove(
				aprHistoryPlot.getLegendWidget());
		aprHistoryPlot.getLayoutManager().remove(
				aprHistoryPlot.getDomainLabelWidget());
		aprHistoryPlot.getLayoutManager().remove(
				aprHistoryPlot.getRangeLabelWidget());

		aprHistoryPlot.setRangeBoundaries(-(orSensor[0].getMaximumRange()),
				orSensor[0].getMaximumRange(), BoundaryMode.FIXED);
		aprHistoryPlot.setDomainBoundaries(0, HISTORY_SIZE, BoundaryMode.FIXED);
		aprHistoryPlot.addSeries(azimuthHistorySeries,
				new LineAndPointFormatter(Color.rgb(100, 100, 200), null, null,
						null));
		aprHistoryPlot.addSeries(pitchHistorySeries, new LineAndPointFormatter(
				Color.rgb(100, 200, 100), null, null, null));
		aprHistoryPlot.addSeries(rollHistorySeries, new LineAndPointFormatter(
				Color.rgb(200, 100, 100), null, null, null));
		aprHistoryPlot.setDomainStepMode(XYStepMode.INCREMENT_BY_VAL);
		aprHistoryPlot.setDomainStepValue(HISTORY_SIZE / 10);
		aprHistoryPlot.setTicksPerRangeLabel(3);

		aprHistoryPlot.setRangeValueFormat(new DecimalFormat("#"));
		aprHistoryPlot.setDomainValueFormat(new DecimalFormat("#"));

		final PlotStatistics histStats = new PlotStatistics(30, false);

		aprHistoryPlot.addListener(histStats);

		for (Sensor sensor : orSensor) {
			sensorMgr.registerListener(this, sensor, SENSOR_RATE); // SENSOR_DELAY_FASTEST
		}

		redrawer = new Redrawer(Arrays.asList(new Plot[] { aprHistoryPlot }),
				100, false);

		mTextView = (TextView) findViewById(R.id.viewTextView);
		mTextView.setText(S_READY);

		mTimeView = (TextView) findViewById(R.id.viewTimeView);
		mTimeView.setText(0 + "");
	}

	private void prepareRecorder() {
		recorder = new MediaRecorder();
		recorder.setPreviewDisplay(holder.getSurface());

		if (usecamera) {
			camera.unlock();
			recorder.setCamera(camera);
		}

		recorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
		recorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);
		recorder.setProfile(camcorderProfile);

		String mOutputFile = mOutputPath + "/" + mNamePrefix
				+ System.currentTimeMillis();

		try {
			recorder.setOutputFile(mOutputFile + mExtension);
		} catch (Exception e) {
			Log.e(LOGTAG, "Failed to find the needed Gyro data file.");
			Toast.makeText(getApplicationContext(),
					"Failed to find the needed Gyro data file.",
					Toast.LENGTH_LONG).show();
			e.printStackTrace();
			finish();
		}

		try {
			recorder.prepare();
		} catch (IllegalStateException e) {
			e.printStackTrace();
			finish();
		} catch (IOException e) {
			e.printStackTrace();
			Log.e(LOGTAG, "Media Recorder Creation Failed.");
			Toast.makeText(getApplicationContext(),
					"Media Recorder Creation Failed.", Toast.LENGTH_LONG)
					.show();
			finish();
		}

		// prepare the sensor recorder
		try {
			for (int i = 0; i < orSensor.length; i++) {
				sensorStream[i] = new DataOutputStream(
						new BufferedOutputStream(new FileOutputStream(
								mOutputFile + "_" + orSensor[i].getType()
										+ ".sen")));
			}

		} catch (IOException ex) {
			ex.printStackTrace();
			Log.e(LOGTAG, "Sensor Recorder Creation Failed.");
			Toast.makeText(getApplicationContext(),
					"Sensor Recorder Creation Failed.", Toast.LENGTH_LONG)
					.show();
			finish();
		}

	}

	public void onClick(View v) {
		if (recording) {
			try {
				for (DataOutputStream stream : sensorStream)
					stream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

			recorder.stop();
			if (usecamera) {
				try {
					camera.reconnect();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			recording = false;
			handler.removeCallbacks(update_runnable);
			mTextView.setText(S_READY);
			Toast.makeText(getApplicationContext(), "Recording Stopped.",
					Toast.LENGTH_SHORT).show();
			Log.v(LOGTAG, "Recording Stopped");
			mTimeView.setText(0 + "");
		} else {
			mTextView.setText(S_RECORDING);
			prepareRecorder();
			recording = true;
			recorder.start();
			Toast.makeText(getApplicationContext(), "Recording Started.",
					Toast.LENGTH_SHORT).show();
			Log.v(LOGTAG, "Recording Started");
			start_time = System.currentTimeMillis();
			updateTextView();
		}
	}

	public void surfaceCreated(SurfaceHolder holder) {
		Log.v(LOGTAG, "surfaceCreated");

		if (usecamera) {
			camera = Camera.open();
			// printCameraProperties();

			try {
				camera.setPreviewDisplay(holder);
				camera.startPreview();
				previewRunning = true;
			} catch (IOException e) {
				Log.e(LOGTAG, e.getMessage());
				e.printStackTrace();
			}
		}

	}

	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		Log.v(LOGTAG, "surfaceChanged");

		if (!recording && usecamera) {
			if (previewRunning) {
				camera.stopPreview();
			}

			try {
				Camera.Parameters p = camera.getParameters();
				p.setPreviewSize(camcorderProfile.videoFrameWidth,
						camcorderProfile.videoFrameHeight);
				p.setVideoStabilization(false);

				camera.setParameters(p);
				camera.setPreviewDisplay(holder);
				camera.startPreview();

				previewRunning = true;
			} catch (IOException e) {
				Log.e(LOGTAG, e.getMessage());
				e.printStackTrace();
			}
		}
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		Log.v(LOGTAG, "surfaceDestroyed");
		if (recording) {
			recorder.stop();
			recording = false;
			recorder.release();
		}
		if (usecamera) {
			previewRunning = false;
			camera.release();
		}
		finish();
	}

	@Override
	public void onResume() {
		super.onResume();
		redrawer.start();
	}

	@Override
	public void onPause() {
		handler.removeCallbacks(update_runnable);
		redrawer.pause();
		super.onPause();
	}

	@Override
	public void onDestroy() {
		redrawer.finish();
		if (sensorMgr != null) {
			sensorMgr.unregisterListener(this);
		}
		super.onDestroy();
	}

	@Override
	public synchronized void onSensorChanged(SensorEvent sensorEvent) {

		if (sensorEvent.sensor == orSensor[0]) { // gyro
			if (rollHistorySeries.size() > HISTORY_SIZE) {
				rollHistorySeries.removeFirst();
				pitchHistorySeries.removeFirst();
				azimuthHistorySeries.removeFirst();
			}

			azimuthHistorySeries.addLast(null, sensorEvent.values[0]);
			pitchHistorySeries.addLast(null, sensorEvent.values[1]);
			rollHistorySeries.addLast(null, sensorEvent.values[2]);
		}

		if (recording) {
			try {
				DataOutputStream outStream = null;
				if (sensorEvent.sensor == orSensor[0]) { // gyro
					outStream = sensorStream[0];
				} else {
					outStream = sensorStream[1];
				}

				float dT = 0.0f;
				if (timestamp == 0) {
					timestamp = sensorEvent.timestamp;
					dT = 0.0f;
				} else {
					dT = (sensorEvent.timestamp - timestamp) * NS2S;
				}				
				
				outStream.writeBytes(dT + " ");
				outStream.writeBytes(sensorEvent.values[0] + " ");
				outStream.writeBytes(sensorEvent.values[1] + " ");
				outStream.writeBytes(sensorEvent.values[2] + "\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int i) {
		// Not interested in this event
	}

	// utility functions
	public static boolean createDirIfNotExists(String path) {
		boolean ret = true;
		File file = new File(path);
		if (!file.exists()) {
			if (!file.mkdirs()) {
				ret = false;
			}
		}
		return ret;
	}

	// to display time.
	Runnable update_runnable = new Runnable() {
		public void run() {
			updateTextView();
		}
	};

	public void updateTextView() {
		long duration = (int) ((System.currentTimeMillis() - start_time) / 1000);
		mTimeView.setText(duration + "");
		handler.postDelayed(update_runnable, 1000);
	}

	public List<Size> getSupportedVideoSizes(Camera camera) {
		if (camera.getParameters().getSupportedVideoSizes() != null) {
			return camera.getParameters().getSupportedVideoSizes();
		} else {
			// Video sizes may be null, which indicates that all the supported
			// preview sizes are supported for video recording.
			return camera.getParameters().getSupportedPreviewSizes();
		}
	}

	@SuppressWarnings("unused")
	private void printCameraProperties() {
		List<Size> size;
		if (camera.getParameters().getSupportedVideoSizes() != null) { // return
			Log.i(LOGTAG, "Recording Supported Video Sizes: ");
			size = camera.getParameters().getSupportedVideoSizes();
			for (Size s : size) {
				Log.i(LOGTAG, "Will Only Record : Width : " + s.width
						+ ", Height : " + s.height);
			}
		}

		// Video sizes may be null, which indicates that all the supported
		// preview sizes are supported for video recording.
		Log.i(LOGTAG, "Preview and Recording Supported Video Sizes: ");
		size = camera.getParameters().getSupportedPreviewSizes();
		for (Size s : size) {
			Log.i(LOGTAG, "Will Preview and Record : Width : " + s.width
					+ ", Height : " + s.height);
		}
	}
}
